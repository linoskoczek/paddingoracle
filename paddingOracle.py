#!/usr/bin/python2

import sys
import base64
import urllib
import requests
import re

def request(_cookie):
    url = '###'
    cookies = dict(auth=_cookie)
    r = requests.get(url, cookies=cookies)
    return r.text


def is_valid(_response):
    if("Invalid padding" in _response):
        return False
    return True


def try_req(_cookie):
    return is_valid(request(_cookie))

def b64d(string):
    return base64.urlsafe_b64decode(string)

def b64(string):
    return urllib.quote(base64.urlsafe_b64encode(string).replace("-", "+").replace("_", "/"))

def progress(min):
    sys.stdout.write("\rChecking %i " % min)
    sys.stdout.flush()

def asserts():
    assert try_req(b64(b64d(cookie)))
    assert try_req(b64(cookie_decoded))
    d = b64d('u7bvLewln6MjLmdoVXwPdW0zA8VPloLVRChmcqmtj+RqsGAShU90sTmA9ETpdQzR')
    e = b64(d)
    assert try_req(e)

cookie = "u7bvLewln6P/UK34FjlbT+LJvDKxHsCN"
cookie_decoded = b64d(cookie)
block_size = 8

single_blocks = [cookie_decoded[i:i+block_size] for i in range(0, len(cookie_decoded), block_size)]
blocks = []
for i in single_blocks:
    blocks.append(list(i))

print blocks

asserts()

### PROGRAM ###

def decrypt():
    complete_result = ''
    for block in xrange(len(blocks) - 1, 0, -1):
        c0 = blocks[block - 1]
        c1 = blocks[block]

        c0_cpy = c0[:]
        c1_cpy = c1[:]

        actual_plaintext = [''] * len(c0)
        intermediate = [-1] * len(c0)

        for index in xrange(len(c0) - 1, -1, -1):
            r_index = len(c0) - index
            potential_plaintext = [r_index] * len(c0)
            correct_result = -1
            for byte in range(256):
                progress(byte)
                c0_cpy[index] = chr(byte)
                base = b64("".join(c0_cpy) + "".join(c1_cpy))
                if(try_req(base)):
                    if(c0[index] != byte):
                        correct_result = byte
                        print "<-- PAD"
                        break
                    print "<-- ORG"

            if(correct_result == -1):
                print "Unexpected! => no result!"

            intermediate[index] = correct_result ^ potential_plaintext[index]
            plaintext = intermediate[index] ^ ord(c0[index])
            actual_plaintext[index] = chr(plaintext)

            ### FIX c0_cpy
            c0_cpy = c0[:]
            for i in xrange(index, len(c0), 1):
                c0_cpy[i] = chr((r_index+1) ^ intermediate[i])
            print c0_cpy

            print "Plaintext found:", plaintext
            print "###", actual_plaintext, "###"
        
        print complete_result.join(actual_plaintext)
        print complete_result
    print "Final: ", complete_result
decrypt()